var express = require('express');
var router = express.Router();

//Models
const BulkDataObject=require('../models/Bulkdata');

/* Bulk Data Collection Route */

router.post('/', function(req, res, next) {
const BulkData=new BulkDataObject(req.body);
const promise = BulkData.save();
promise.then((data)=>{
  res.status(200).json({
    success:true,
    message:"Salute Australia! We saved your data successfully!"
  });
}).catch((err)=>{
  //We can add some error handling here. 
  res.status(500).json(err);
});
});

module.exports = router;
